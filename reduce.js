const customReduce = (items, cb) => {
    let calculatedValue = 0
    if (items.length != 0) {
        for (let index = 0; index < items.length; index++) {
            calculatedValue = cb(calculatedValue, items[index], index, items)
        }
        return calculatedValue;
    }
}

module.exports = customReduce;