const customMap = (items, cb) =>{
    const result = [];          //array to store manipulated values

    for(let index = 0; index < items.length; index++)
    {
        //passing three arguments is not necessary but it has been passed just to show the working of the callback function
        let ans = cb(items[index], index, items)
        result.push(ans)
    }
    return result;
}

module.exports = customMap