const customFind = (items, cb) => {

    for (let index = 0; index < items.length; index++) {
        if (cb(items[index], index, items))
            return items[index]
    }
    return undefined;
}

module.exports = customFind