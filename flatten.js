const customFlatten = (items) => {

    //Initialising an empty array which will store the flattened array
    let flattenedArray = [];

    //Iterate through each element of array
    items.forEach(element => {
        if (Array.isArray(element)) {
            flattenedArray = flattenedArray.concat(customFlatten(element))      //if the element is array recursively call the customFlatten() function
        }
        else {
            flattenedArray.push(element)            //if it's not an array push it into the flattened array
        }
    });

    return flattenedArray;
}

module.exports = customFlatten;