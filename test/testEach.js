const items = [1, 2, 3, 4, 5, 5]

function cb(element, index) {
    console.log(`Element at ${index} is ${element}`)
}

const sol1 = require('../each')
sol1(items,cb)