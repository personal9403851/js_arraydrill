const customFlatten = require('../flatten')

const items = [1, [2], [[3]], [[[4]]]];

const result = customFlatten(items)
console.log(result)