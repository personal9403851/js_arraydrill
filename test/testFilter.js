const customFilter = require('../filter')

const items = [1, 2, 3, 4, 5, 5];

function cb(num, index, items) {
    if (num >= 3) {
        return true;
    }
}

const result = customFilter(items, cb)
console.log(result)