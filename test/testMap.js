const customMap = require('../map')

const items = [1, 2, 3, 4, 5, 5]

//Call back function which calculates the square of the items array and store it into new array
function cb(num,index,arr){
    return num * num
}

//Calling customMap function to create a new array named itemsNew with the changed value
let itemsNew = customMap(items, cb)
console.log(itemsNew)