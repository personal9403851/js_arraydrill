const customFilter = (items, cb) => {

    const filteredArray = [];
    for (let index = 0; index < items.length; index++) {
        if (cb(items[index], index, items)) {
            filteredArray.push(items[index])
        }
    }
    return filteredArray;
}

module.exports = customFilter